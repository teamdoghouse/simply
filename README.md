# Simply #

Simple framework for and from Doghouse.

## Changelog

## 1.5.3 (22 apr 2021)

* Bugfix in Pagination (countable)

## 1.5.2 (26 jan 2020)

* Database converts empty values to NULL

## 1.5.0 (30 jan 2019)

* Database: new method: "select", to select rows
* Database: all methods can return debug_backtrace

### 1.4.9 (16 jan 2018)

* Database has new method: "run", for general queries
* Mail-class checks old and new namespace
* Phpmailer accepts non-verified SSL

### 1.4.8 (10 nov 2017)

* Bugfix: array defined as string caused a boo-boo

### 1.4.5 (25 may 2017)

* Bugfix: Router-class did not work with "foo-bar"

### 1.4.3 (22 may 2017)

* Pagination-class now has functions for prev and next page

### 1.4.1 (18 april 2017)

* Always a bugfix.

### 1.4.0 (18 april 2017)

* Database now supports setting timezone

### 1.3.9 (1 april 2017)

* Bugfix in router-class, calling non-existing Layout-method

### 1.3.8 (24 march 2017)

* Bugfix in router-class

### 1.3.7 (24 march 2017)

* Added the Router-class

### 1.3.6 (9. march 2017)

* Corrected bug in Database::delete

### 1.3.5 (9. march 2017)

* New methods in Database: delete

### 1.3.4 (20. jan 2017)

* New methods in Database: insert and update

### 1.3 (2. may 2016)

* Autoloader added.
* SMS-function now requires a API url.
* Packages removed. Other software should be installed using Composer etc.

### 1.2-8 (11. january 2016)

* Mail-class now has a public function addEmbeddedImage($filename,$cid,$name).
* printr-functions now knows if the script is run in command line or not.

### 1.2-7 (25. september 2014)

* New class: QueryString
* Changes in Calendar-class: get() returns both the rows and the first and last date of the view (also dates outside the current month)
* Changed: Tools::humanFilesize changed from function to public static function

### 1.2-6 (14. september 2014)

* New class: SMS
* New function: Tools::humanFilesize($bytes, $decimals = 2)
* Bugfix: class.calendar.php - missing reference to global namespace on Datetime-object
* Bugfix: class.mail.php linje 34 - default charset set to UTF-8
* Bugfix: class.mail.php linje 69 - did not return ErrorInfo from PhpMailer
* Bugfix: class.pdoquery.php linje 29 - __construct is no longer a static function

### 1.2-5 (28. august 2014)

* New class: Calendar

### 1.2-4 (19. august 2014)

* Corrected name of the class pagnation

### 1.2-3 (27. july 2014)

* New class: Pagnation

### 1.2-2 (26. july 2014)

* Mail-klasse. Se wiki for bruk.

### 1.2-1 (??)

Missing changelog.

### 1.1-1 (22. juli 2014)

* NEW: Mobile Detect v 2.8.3
* PHPasslib v 3.0
