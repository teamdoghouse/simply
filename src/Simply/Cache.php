<?php

class Cache
{
    static public function getInstance($name = null)
    {
        // Allow passing in a name to get multiple instances
        // If you do not pass a name, it functions as a singleton
        
        $name = "cache";
        
        if (!Registry::contains($name))
        {
            $instance = new Memcached();
            $instance->addServer("localhost", 11211);
            Registry::add($instance, $name);
        }

        return Registry::get($name);
    }
    
    static public function get($name)
    {
        $m = self::getInstance();
        return $m->get($name);
    }

    static public function set($name, $object, $expiration=0)
    {
        $m = self::getInstance();
        return $m->set($name, $object, $expiration);
    }

    static public function delete($name)
    {
        $m = self::getInstance();
        return $m->delete($name);
    }
}

?>