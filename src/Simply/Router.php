<?php

namespace Simply;

class Router
{
    private $ajax = false;
    private $routes;
    private $attr;
    private $url;
    private $urlArray;
    private $defaultPage;
    private $callback;

    function __construct()
    {
        $url = strtok($_SERVER["REQUEST_URI"],'?');
        $url = rtrim($url, '/') . "/";
        $basePath = str_replace("/index.php", "", $_SERVER['SCRIPT_NAME']);
        $url = str_replace($basePath, '', $url);
        $url_array = explode("/", $url);
        array_splice($url_array, 0, 1);
        $request_array = array_filter($url_array);

        if ($request_array[0] == "ajax") {
            $this->ajax = true;
        }

        $this->url = $url;
        $this->urlArray = $request_array;

        \Simply\Registry::replace($this->url, "route-url");
        \Simply\Registry::replace($this->urlArray, "route-urlArray");
    }

    public static function staticUrl($attr = NULL)
    {
        $url = strtok($_SERVER["REQUEST_URI"],'?');
        $url = rtrim($url, '/') . "/";
        $basePath = str_replace("/index.php", "", $_SERVER['SCRIPT_NAME']);
        $url = str_replace($basePath, '', $url);
        $url_array = explode("/", $url);
        array_splice($url_array, 0, 1);
        $request_array = array_filter($url_array);

        if ($attr == "url") {
            return $url;
        }

        else if ($attr == "urlArray") {
            return $request_array;
        }

        else {
            return ['url' => $url, 'urlArray' => $request_array];
        }
    }

    public function default($page)
    {
        $this->defaultPage = $page;
    }

    public function callback($fn)
    {
        $this->callback = $fn;
    }

    public function add($path, $page, $params = NULL)
    {
        $pathArray = explode("/", $path);

        if (!is_array($pathArray)) {
            return false;
        }

        $patternArray = array();

        $i = 0;
        foreach ($pathArray as $part)
        {
            if (preg_match("(\\{.*?\\})", $part)) {

                if (strpos($part, "[numeric]")) {
                    $part = str_replace("[numeric]", NULL, $part);
                    $partPattern = "(\\d+)";
                }

                else {
                    $part = str_replace("[numeric]", NULL, $part);
		    $partPattern = "([^/]+)";
                }
            }

            else {
                $partPattern = $part;
            }

            $patternArray[] = $partPattern;

            $i++;
        }

        $pattern = '#^' . implode("/", $patternArray) . '/$#';
        $this->routes[] = ["path" => $path, "pattern" => $pattern, "page" => $page, "params" => $params];
    }

    public static function redirect($location)
    {
        header("Location: " . $location);
        exit;
    }

    public static function reload()
    {
        header("Refresh:0");
        exit;
    }

    public static function getAttribute($name)
    {
        try {
            $attr = \Simply\Registry::get("route-attributes");
        } catch (Exception $e) {
            return false;
        }

        if (isset($attr[$name]))
        {
            return $attr[$name];
        }

        return false;
    }

    public static function getAttributes()
    {
        try {
            $attr = \Simply\Registry::get("route-attributes");
        } catch (Exception $e) {
            return false;
        }

        if (is_array($attr))
        {
            return $attr;
        }

        return false;
    }

    public static function getParam($name)
    {
        try {
            $params = \Simply\Registry::get("route-params");
        } catch (Exception $e) {
            return false;
        }

        if (isset($params[$name]))
        {
            return $params[$name];
        }

        return false;
    }

    public static function getParams()
    {
        try {
            $params = \Simply\Registry::get("route-params");
        } catch (Exception $e) {
            return false;
        }

        if (is_array($params))
        {
            return $params;
        }

        return false;
    }

    public function run()
    {
        if (count($this->urlArray) == 0) {
            call_user_func($this->callback, $this->defaultPage);
            return true;
        }

        foreach ($this->routes as $data)
        {
            if ( preg_match($data['pattern'], $this->url) ) {

                $pathArray = explode("/", $data['path']);

                $i = -1;
                foreach ($pathArray as $part)
                {
                    if (preg_match("(\\{.*?\\})", $part)) {
                        $part = str_replace("[numeric]", NULL, $part);
                        $part = str_replace(['{','}'], NULL, $part);

                        $this->attr[$part] = $this->urlArray[$i];
                    }

                    $i++;
                }

                \Simply\Registry::replace($data['page'], "route-page");
                \Simply\Registry::replace($this->attr, "route-attributes");
                \Simply\Registry::replace($data['params'], "route-params");

                call_user_func($this->callback, $data['page']);
                return false;
            }
        }

        call_user_func($this->callback, "404");
        return false;
    }

    /* Static functions */

    public static function getUrl()
    {
        return $_SERVER['REQUEST_SCHEME'] . "://" . $_SERVER['HTTP_HOST'] . str_replace("/index.php", "", $_SERVER['SCRIPT_NAME']);
    }
}
