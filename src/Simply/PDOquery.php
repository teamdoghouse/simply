<?php

namespace Simply;

class PDOquery
{
    /**
     *
     * @var string
     */
    private $query;
    
    /**
     *
     * @var string
     */
    private $printquery;
    
    /**
     *
     * @var \PDOStatement
     */
    private $pdo;

    /**
     * 
     * @param string $query
     */
    public function __construct($query)
    {
        $dbh = \Simply\Database::getInstance();

        $this->query = $query;
        $this->printquery = $this->query;
        $this->pdo = $dbh->prepare($this->query);
    }
    
    public static function beginTransaction()
    {
        $dbh = \Simply\Database::getInstance();
        $dbh->beginTransaction();
    }
    
    public static function commit()
    {
        $dbh = \Simply\Database::getInstance();
        $dbh->commit();
    }
    
    public static function rollBack()
    {
        $dbh = \Simply\Database::getInstance();
        $dbh->rollBack();
    }
    
    public static function lastInsertId()
    {
        $dbh = \Simply\Database::getInstance();
        return $dbh->lastInsertId();
    }
    
    /**
     * 
     * @param mixed $parameter
     * @param mixed $value
     * @param int $data_type
     */
    public function bindValue($parameter, $value, $data_type = null)
    {
        if (is_null($data_type))
        {
            switch (true)
            {
                case is_int($value):
                    $data_type = \PDO::PARAM_INT;
                    break;
                case is_bool($value):
                    $data_type = \PDO::PARAM_BOOL;
                    break;
                case is_null($value):
                    $data_type = \PDO::PARAM_NULL;
                    break;
                default:
                    $data_type = \PDO::PARAM_STR;
            }
        }

        if (empty($value)) {
            $value = NULL;
        }

        $this->pdo->bindValue($parameter, $value, $data_type);
        $this->printquery = str_replace($parameter, "'" . $value . "'", $this->printquery);
    }
    
    /**
     * 
     * @param type $parameter
     * @param type $variable
     * @param type $data_type
     */
    public function bindParam($parameter, &$variable, $data_type = null)
    {
        if (is_null($data_type))
        {
            switch (true)
            {
                case is_int($value):
                    $data_type = \PDO::PARAM_INT;
                    break;
                case is_bool($value):
                    $data_type = \PDO::PARAM_BOOL;
                    break;
                case is_null($value):
                    $data_type = \PDO::PARAM_NULL;
                    break;
                default:
                    $data_type = \PDO::PARAM_STR;
            }
        }

        $this->pdo->bindParam($parameter, $variable, $data_type);
        $this->printquery = str_replace($parameter, "'" . $variable . "'", $this->query);
    }
    
    public function getQuery()
    {
        return $this->printquery;
    }
    
    public function execute(array $args=null)
    {
        $dbh = \Simply\Database::getInstance();

        $result['success'] = false;

        $this->pdo->execute();
        
        if ($this->pdo->errorCode() == "00000")
        {
            $result['success'] = true;
            $result['rowCount'] = $this->pdo->rowCount();
            $result['lastInsertId'] = $dbh->lastInsertId();
            
            if ($args['array']) { $result['data'] = $this->pdo->fetchAll(\PDO::FETCH_ASSOC);     }
            else { $result['data'] = $this->pdo->fetchAll(\PDO::FETCH_OBJ); }
        }
        
        else
        {
            $error = implode(" | ", $this->pdo->errorInfo());
            error_log("PDOERROR: {$error}");
            $result['error'] = $this->pdo->errorInfo();
        }
        
        return $result;
    }
}

