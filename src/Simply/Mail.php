<?php

namespace Simply;

class Mail
{
    public function __construct($smtp, $from, $fromName, $port = 25)
    {
        if (!isset($smtp) || empty($smtp))
        {
            die("No SMTP-server given");
        }

        if (!isset($from) || empty($from))
        {
            die("No from address given");
        }

        if (!isset($fromName) || empty($fromName))
        {
            die("No from name given");
        }

        if (class_exists("PHPMailer")) {
            $this->phpmailer = new \PHPMailer;
        }

        else if (class_exists("\PHPMailer\PHPMailer\PHPMailer")) {
            $this->phpmailer = new \PHPMailer\PHPMailer\PHPMailer;
        }

        else {
            die("PHPMailer not loaded");
        }

        $this->phpmailer->isSMTP();
        $this->phpmailer->isHTML(true);
        $this->phpmailer->Port = $port;
        $this->phpmailer->Host = $smtp;
        $this->phpmailer->From = $from;
        $this->phpmailer->FromName = $fromName;
        $this->phpmailer->CharSet = "UTF-8";
        $this->phpmailer->SMTPOptions = [
            "ssl" => [
                "verify_peer" => false,
                "verify_peer_name" => false,
                "allow_self_signed" => true
            ]
        ];
    }

    public function addAddress($address,$name=NULL)
    {
        $this->phpmailer->addAddress($address, $name);
    }

    public function addCC($address,$name=NULL)
    {
        $this->phpmailer->addCC($address, $name);
    }

    public function addBCC($address,$name=NULL)
    {
        $this->phpmailer->addBCC($address, $name);
    }

    public function addAttachment($attachment,$altname=NULL)
    {
        $this->phpmailer->addAttachment($attachment,$altname);
    }

    public function addEmbeddedImage($filename,$cid,$name)
    {
        $this->phpmailer->addEmbeddedImage($filename,$cid,$name);
    }

    public function send($subject,$body)
    {
        $this->phpmailer->Subject = $subject;
        $this->phpmailer->Body = $body;

        if ($this->phpmailer->send())
        {
            return array("success" => true);
        }

        else
        {
            return array("success" => false, "error" => $this->phpmailer->ErrorInfo);
        }
    }
}

?>