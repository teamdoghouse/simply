<?php

namespace Simply;

class SMS
{
    public $username;
    public $password;
    public $sender;
    public $numbers;
    public $content;

    function __construct($username,$password)
    {
        $this->username = $username;
        $this->password = $password;
    }

    public function sender($var)
    {
        $this->sender = $var;
    }

    public function content($var)
    {
        $this->content = $var;
    }

    public function addNumber($n)
    {
        if (!is_numeric($n))
        {
            return false;
        }

        if (strlen($n) != 8)
        {
            return false;
        }

        $this->numbers[] = ("47" . $n);
    }

    public function send()
    {
        if (!function_exists('curl_init')){ return array("success" => false, "error" => "CURL is not installed"); }
        if (!defined('DOGHOUSE_SMS_API_URL')){ return array("success" => false, "error" => "CURL is not installed"); }

        if (empty($this->username)) { return array("success" => false, "error" => "Missing username"); }
        if (empty($this->password)) { return array("success" => false, "error" => "Missing password"); }
        if (empty($this->sender)) { return array("success" => false, "error" => "Missing sender"); }
        if (empty($this->content)) { return array("success" => false, "error" => "Missing content"); }
        if (empty($this->username)) { return array("success" => false, "error" => "Missing username"); }
        if (empty($this->numbers)) { return array("success" => false, "error" => "Missing numbers"); }

        $url = ("username={$this->username}&password={$this->password}&sender={$this->sender}&destination=" . implode(",", $this->numbers) . "&content=" . urlencode($this->content));

        $ch = curl_init();
        curl_setopt($ch, CURLOPT_CUSTOMREQUEST, 'GET');
        curl_setopt($ch, CURLOPT_URL, (DOGHOUSE_SMS_API_URL . "?" . $url));
        curl_setopt($ch, CURLOPT_HEADER, 0);
        curl_setopt($ch, CURLOPT_RETURNTRANSFER, true);
        curl_setopt($ch, CURLOPT_TIMEOUT, 20);
        curl_setopt($ch, CURLOPT_SSL_VERIFYPEER, false);
        $output = curl_exec($ch);
        curl_close($ch);
        return json_decode($output, true);
    }
}

?>