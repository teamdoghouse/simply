<?php

namespace Simply;

class Validate {

    /**
     * 
     * @param string $email
     * @return boolean
     */
    public static function email($email) {
        return (filter_var($email, FILTER_VALIDATE_EMAIL)) ? true : false;
    }
    
    /**
     * 
     * @param mixed $value
     * @param int $min
     * @param int $max
     * @return boolean
     */
    public static function int($value,$min=null, $max=null){
        
        $options = array(
            "options" => null
        );
        
        if(!is_null($min) && self::int($min)){
            if(!is_array($options["options"])){
                $options["options"] = array();
            }
            $options["options"]["min_range"] = $min;
        }
        
        if(!is_null($max) && self::int($max)){
            if(!is_array($options["options"])){
                $options["options"] = array();
            }
            $options["max_range"] = $max;
        }
        
        return(filter_var($value, FILTER_VALIDATE_INT, $options)!==false)?true:false;
    }
    
    
    /**
     * 
     * @param mixed $value
     * @param int $min
     * @param int $max
     * @return boolean
     */
    public static function float($value,$min=null, $max=null){
        
        
        $value_validation = (filter_var($value, FILTER_VALIDATE_FLOAT)!==false)?true:false;
        $min_validation = true;
        $max_validation = true;
        
                
        if($value_validation && !is_null($min) && self::float($min)){
            if($value < $min){
                $min_validation = false;
            }
        }
        
        if($value_validation && !is_null($max) && self::int($max)){
            if($value > $max) {
                $max_validation = false;
            }
        }
        
        return($value_validation && $min_validation && $max_validation)?true:false;
    }
    
    
    /**
     * 
     * @param string $date
     * @param string $format
     * @return boolean
     */
    public static function date($date, $format = "Y-m-d"){
        $d = DateTime::createFromFormat($format, $date);
        return ($d && $d->format($format) == $date);
    }

}
