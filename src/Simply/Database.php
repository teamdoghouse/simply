<?php

namespace Simply;

class Database extends \PDO
{
    public function __construct()
    {
        define("MYSQL_DSN", "mysql:host=" . MYSQL_HOSTNAME . ";dbname=" . MYSQL_DATABASE);

        if (defined("MYSQL_TIMEZONE")) {
            $options = [ \PDO::MYSQL_ATTR_INIT_COMMAND => "SET NAMES utf8, time_zone = '" . MYSQL_TIMEZONE . "'" ];
        }

        else {
            $options = [ \PDO::MYSQL_ATTR_INIT_COMMAND => "SET NAMES utf8" ];
        }

        parent::__construct(
            MYSQL_DSN,
            MYSQL_USERNAME,
            MYSQL_PASSWORD,
            $options
        );
    }

    static public function getInstance($name = null)
    {
        $class = get_called_class();

        $name = (!is_null($name)) ? $name : $class;
        if (!Registry::contains($name)) {
            $instance = new $class();
            Registry::add($instance, $name);
        }
        return Registry::get($name);
    }

    public static function insert($table, $data, $options = NULL)
    {
        if (empty($table)) {
            return ['success' => false, 'error' => 'Missing table'];
        }

        if (!is_array($data)) {
            return ['success' => false, 'error' => 'Missing data'];
        }

        $fields = array();
        $placeholders = array();
        $values = array();

        foreach ($data as $key => $val)
        {
            if (!isset($val) || empty($val)) {
                $val = NULL;
            }

            $fields[] = "`{$key}`";
            $placeholders[] = ":{$key}";
            $values[] = $val;
        }

        $sql = "INSERT INTO `{$table}` (" . implode(",", $fields) . ") VALUES (" . implode(",", $placeholders) . ")";

        $pdo = new \Simply\PDOquery($sql);

        foreach ($data as $key => $val)
        {
            $pdo->bindValue(":{$key}", $val);
        }

        $pdoresult = $pdo->execute();

        if (is_array($options) && $options['getQuery']) {
            $pdoresult['query'] = $pdo->getQuery();
        }

        if (is_array($options) && $options['debug']) {
            $pdoresult['debug'] = debug_backtrace();
        }

        return $pdoresult;
    }

    public static function update($table, $update, $where, $options = NULL)
    {
        if (empty($table)) {
            return ['success' => false, 'error' => 'Missing table'];
        }

        if (!is_array($update)) {
            return ['success' => false, 'error' => 'Missing update data'];
        }

        if (!is_array($where)) {
            return ['success' => false, 'error' => 'Missing where data'];
        }

        $bind = array();

        /* Handle update data */

        $updateFieldsAndValues = array();

        foreach ($update as $key => $val)
        {
            if (!isset($val) || empty($val)) {
                $val = NULL;
            }

            $updateFieldsAndValues[] = "`{$key}` = :{$key}";
            $bind[$key] = $val;
        }

        /* Handle update data */

        $whereFieldsAndValues = array();

        foreach ($where as $key => $val)
        {
            if (!isset($val) || empty($val)) {
                $val = NULL;
            }

            $whereFieldsAndValues[] = "`{$key}` = :{$key}";
            $bind[$key] = $val;
        }

        /* Complete query */

        $sql = "UPDATE `{$table}` SET " . implode(", ", $updateFieldsAndValues) . " WHERE " . implode(" AND ", $whereFieldsAndValues);

        $pdo = new \Simply\PDOquery($sql);

        foreach ($bind as $key => $val)
        {
            $pdo->bindValue(":{$key}", $val);
        }

        $pdoresult = $pdo->execute();

        if (is_array($options) && $options['getQuery']) {
            $pdoresult['query'] = $pdo->getQuery();
        }

        if (is_array($options) && $options['debug']) {
            $pdoresult['debug'] = debug_backtrace();
        }

        return $pdoresult;
    }

    public static function delete($table, $where, $options = NULL)
    {
        if (empty($table)) {
            return ['success' => false, 'error' => 'Missing table'];
        }

        if (!is_array($where)) {
            return ['success' => false, 'error' => 'Missing where data'];
        }

        $bind = array();

        /* Handle where data */

        $whereFieldsAndValues = array();

        foreach ($where as $key => $val)
        {
            $whereFieldsAndValues[] = "`{$key}` = :{$key}";
            $bind[$key] = $val;
        }

        /* Complete query */

        $sql = "DELETE FROM `{$table}` WHERE " . implode(" AND ", $whereFieldsAndValues);

        $pdo = new \Simply\PDOquery($sql);

        foreach ($bind as $key => $val)
        {
            $pdo->bindValue(":{$key}", $val);
        }

        $pdoresult = $pdo->execute();

        if (is_array($options) && $options['getQuery']) {
            $pdoresult['query'] = $pdo->getQuery();
        }

        if (is_array($options) && $options['debug']) {
            $pdoresult['debug'] = debug_backtrace();
        }

        return $pdoresult;
    }

    public static function run($sql, $values = NULL, $options = NULL)
    {
        $pdo = new \Simply\PDOquery($sql);

        if (is_array($values) && count($values)) {

            foreach ($values as $k => $v)
            {
                $pdo->bindValue(":{$k}", $v);
            }
        }

        $pdoresult = $pdo->execute();

        if (is_array($options) && $options['getQuery']) {
            $pdoresult['query'] = $pdo->getQuery();
        }

        if (is_array($options) && $options['debug']) {
            $pdoresult['debug'] = debug_backtrace();
        }

        return $pdoresult;
    }

    public static function select($fields, $from, $where = NULL, $order = NULL, $sort = 'ASC', $options = NULL)
    {
        $sql = "SELECT ";

        if (is_array($fields)) {
            
            $i = 0;
            foreach ($fields as $field)
            {
                if ($i > 0) {
                    $sql .= ", ";
                }

                $sql .= "`{$field}`";
                $i++;
            }
        }

        else {
            $sql .= "{$fields}";
        }

        $sql .= " FROM `{$from}`";

        if (is_array($where)) {
            
            $sql .= " WHERE";

            $i = 0;
            foreach ($where as $key => $value)
            {
                if ($i > 0) {
                    $sql .= " AND ";
                }

                $sql .= " `{$key}` = :{$key}";
                $i++;

                $sqldata[$key] = $value;
            }
        }

        if ($order) {
            $sql .= " ORDER BY `{$order}` {$sort}";
        }

        $pdoresult = self::run($sql, $sqldata, $options);

        return $pdoresult;
    }
}
