<?php

namespace Simply;

class QueryString
{    
    public static function getAsArray(array $removeFilter = null, array $setParams = null)
    {
        $params = $_GET;
        if(is_array($removeFilter) && sizeof($removeFilter)>0){
            foreach ($removeFilter as $key) {
                unset($params[$key]);
            }
        }
        if(is_array($setParams) && sizeof($setParams)>0){
            foreach ($setParams as $key => $value) {
                $params[$key] = $value;
            }
        }
        return $params;
    }

    public static function getAsString(array $removeFilter = null, array $setParams = null, $addQMark = false)
    {
        $params = self::getAsArray($removeFilter, $setParams);
        return (($addQMark)?"?":"").http_build_query($params);
    }
}

?>