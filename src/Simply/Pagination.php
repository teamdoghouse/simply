<?php

namespace Simply;

class Pagination
{
    private $data; // All the data
    private $per_page; // Items per page
    private $page; // Current page
    private $total_count; // Total number of items
    private $pages; // Number of pages
    private $page_data; // The data of the current page

    function __construct($data, $per_page, $page)
    {
        if (empty($page)) {
            $page = 1;
        }

        $this->data = $data;
        $this->per_page = $per_page;
        $this->page = $page;

        $min = (($page*$per_page)-$per_page)+1;
        $max = $page*$per_page;
        $count = 1;
        $page_data = array();

        $this->total_count = 0;
        $this->pages = 0;

        if (is_array($data) && count($data) > 0)
        {
            foreach($data as $item)
            {
                if($count >= $min && $count <= $max)
                {
                    $page_data[] = $item;
                }

                $count++;
            }

            $this->total_count = count($data);
            $this->pages = ceil(count($data)/$per_page);
        }

        $this->page_data = $page_data;
    }

    public function currentPage()
    {
        return $this->page;
    }

    public function nextPage()
    {
        if ($this->pages > $this->page) {
            return $this->page + 1;
        }

        return false;
    }

    public function prevPage()
    {
        if ($this->page != 0) {
            return $this->page - 1;
        }

        return false;
    }

    public function numberOfPages()
    {
        return $this->pages;
    }

    public function currentPageData()
    {
        return $this->page_data;
    }

    public function totalNumberOfItems()
    {
        return $this->total_count;
    }
}
