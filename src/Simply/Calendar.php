<?php

namespace Simply;

class Calendar
{
    private $year;
    private $month;

    function __construct($year,$month)
    {
        $this->year = $year;
        $this->month = $month;
    }
    
    public static function dayNoToName($day)
    {
        $arr1 = array(1,2,3,4,5,6,7);
        $arr2 = array("Mandag", "Tirsdag", "Onsdag", "Torsdag", "Fredag", "Lørdag", "Søndag");
        return str_replace($arr1, $arr2, $day);
    }
    
    public function next()
    {
        if ($this->month == 12)
        {
            $result['month'] = 1;
            $result['year'] = $this->year + 1;
        }
        
        else
        {
            $result['month'] = $this->month + 1;
            $result['year'] = $this->year;
        }
        
        return (object) $result;
    }
    
    public function prev()
    {
        if ($this->month == 1)
        {
            $result['month'] = 12;
            $result['year'] = $this->year - 1;
        }
        
        else
        {
            $result['month'] = $this->month - 1;
            $result['year'] = $this->year;
        }
        
        return (object) $result;
    }
    
    public function get()
    {
        /* Days in month */
        
        $daysInMonth = cal_days_in_month(CAL_GREGORIAN, $this->month, $this->year); // 31
        
        /* First day of month */
        
        $firstDayOfMonth = new \DateTime("{$this->year}-{$this->month}-01");
        $firstDayOfMonthName = $firstDayOfMonth->format("D");

        switch ($firstDayOfMonthName)
        {
            case "Mon":
                $daysBeforeMonthStart = 0;
            break;
            case "Tue":
                $daysBeforeMonthStart = 1;
            break;
            case "Wed":
                $daysBeforeMonthStart = 2;
            break;
            case "Thu":
                $daysBeforeMonthStart = 3;
            break;
            case "Fri":
                $daysBeforeMonthStart = 4;
            break;
            case "Sat":
                $daysBeforeMonthStart = 5;
            break;
            case "Sun":
                $daysBeforeMonthStart = 6;
            break;
        }
        
        $cols = array();
        
        if ($daysBeforeMonthStart > 0)
        {
            $dt = new \DateTime("{$this->year}-{$this->month}-01");

            $i = 1;
            while ($i <= $daysBeforeMonthStart)
            {
                $dt->modify("-1 day");

                $cols[] = (object) array("date" => $dt->format("Y-m-d"), "dateday" => $dt->format("d"), "dayno" => $dt->format("N"), "dayname" => self::dayNoToName($dt->format("N")), "inMonth" => false);
                $i++;
            }
            
            $cols = array_reverse($cols);
        }
        
        $dt = new \DateTime("{$this->year}-{$this->month}-01");
        
        $i = 1;
        while ($i <= $daysInMonth)
        {
            $cols[] = (object) array("date" => $dt->format("Y-m-d"), "dateday" => $dt->format("d"), "dayno" => $dt->format("N"), "dayname" => self::dayNoToName($dt->format("N")), "inMonth" => true);
            $dt->modify("+1 day");
            $i++;
        }
        
        /* Days left in week */
        
        $tmprows = array_chunk($cols, 7);
        if (count(end($tmprows)) < 7)
        {
            $daysLeft = 7 - count(end($tmprows));
            
            $i = 1;
            while ($i <= $daysLeft)
            {
                $cols[] = (object) array("date" => $dt->format("Y-m-d"), "dateday" => $dt->format("d"), "dayno" => $dt->format("N"), "dayname" => self::dayNoToName($dt->format("N")), "inMonth" => false);
                $dt->modify("+1 day");
                $i++;
            }
        }
        
        $lasti = count($cols) - 1;
        
        $return['first_date'] = $cols[0]->date;
        $return['last_date'] = $cols[$lasti]->date;
        
        /* Split into weeks */
        
        $return['rows'] = array_chunk($cols, 7);
        
        /* Return */

        return $return;
    }
}

?>
