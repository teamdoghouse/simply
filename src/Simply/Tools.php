<?php

namespace Simply;

class Tools
{
    public static function randomString($length = "")
    {	
        $code = md5(uniqid(rand(), true));

        if ($length != "")
        {
            return substr($code, 0, $length);
        }
        
        else
        {
            return $code;
        }
    }
    
    public static function numberformat($number,$decimals=2,$dec_point=',',$thousands_sep=' ')
    {
        return number_format($number, $decimals, $dec_point, $thousands_sep);
    }
    
    public static function dateformat($date=NULL,$format='Y-m-d')
    {
        if (!empty($date))
        {
            $tmp = new \DateTime($date);
            return $tmp->format($format);
        }
        
        else
        {
            return null;
        }
    }
    
    public static function humanFilesize($bytes, $decimals = 2)
    {
        $size = array('B','kB','MB','GB','TB','PB','EB','ZB','YB');
        $factor = floor((strlen($bytes) - 1) / 3);
        return sprintf("%.{$decimals}f", $bytes / pow(1024, $factor)) . @$size[$factor];
    }
}

?>
